#!/usr/bin/env python3
import struct
import time
import sys
import argparse
from Crypto.Cipher import AES
from bluepy.btle import Peripheral, DefaultDelegate, ADDR_TYPE_RANDOM, BTLEGattError

import logging
from datetime import datetime, timedelta

''' TODO
Key should be generated and stored during init
'''
UUID_SVC_MIBAND1 = "0000fee000001000800000805f9b34fb"
UUID_SVC_MIBAND2 = "0000fee100001000800000805f9b34fb"
UUID_CHAR_AUTH = "00000009-0000-3512-2118-0009af100700"
UUID_SVC_ALERT = "0000180200001000800000805f9b34fb"
UUID_CHAR_ALERT = "00002a0600001000800000805f9b34fb"
UUID_SVC_HEART_RATE = "0000180d00001000800000805f9b34fb"
UUID_CHAR_HRM_MEASURE = "00002a3700001000800000805f9b34fb"
UUID_CHAR_HRM_CONTROL = "00002a3900001000800000805f9b34fb"
UUID_CHAR_SENSOR = "00000001-0000-3512-2118-0009af100700"


'''
BLE report
00001530-0000-3512-2118-0009af100700 Proprietary
00001800-0000-1000-8000-00805f9b34fb Generic Access
00001801-0000-1000-8000-00805f9b34fb Generic Attribute
00001802-0000-1000-8000-00805f9b34fb Immediate Alert
0000180a-0000-1000-8000-00805f9b34fb Device Information
0000180d-0000-1000-8000-00805f9b34fb Heart Rate
00001811-0000-1000-8000-00805f9b34fb Alert Notification Service
0000fee0-0000-1000-8000-00805f9b34fb Unknown
0000fee1-0000-1000-8000-00805f9b34fb Unknown
'''

HRM_COMMAND = 0x15
HRM_MODE_SLEEP = 0x00
HRM_MODE_CONTINUOUS = 0x01
HRM_MODE_ONE_SHOT = 0x02

CCCD_UUID = 0x2902

DATA_TYPE_NONE = 0x00
DATA_TYPE_PPG = 0x01
DATA_TYPE_ACC = 0x02


class MiBand2(Peripheral):
    _KEY = b'\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x40\x41\x42\x43\x44\x45'
    _send_key_cmd = struct.pack('<18s', b'\x01\x08' + _KEY)
    _send_rnd_cmd = struct.pack('<2s', b'\x02\x08')
    _send_enc_key = struct.pack('<2s', b'\x03\x08')

    def __init__(self, addr):
        Peripheral.__init__(self, addr, addrType=ADDR_TYPE_RANDOM, iface=0)
        logging.info("Connected")

        svc = self.getServiceByUUID(UUID_SVC_MIBAND2)
        logging.debug("UUID_SVC_MIBAND2: %s", svc)
        self.char_auth = svc.getCharacteristics(UUID_CHAR_AUTH)[0]
        logging.debug("char_auth: %s", self.char_auth)
        self.cccd_auth = self.char_auth.getDescriptors(forUUID=CCCD_UUID)[0]

        svc = self.getServiceByUUID(UUID_SVC_ALERT)
        logging.debug("UUID_SVC_ALERT: %s", svc)
        self.char_alert = svc.getCharacteristics(UUID_CHAR_ALERT)[0]
        logging.debug(
            "char_alert: %s [%x]", self.char_alert, self.char_alert.getHandle())

        svc = self.getServiceByUUID(UUID_SVC_HEART_RATE)
        logging.debug("UUID_SVC_HEART_RATE: %s", svc)
        self.char_hrm_ctrl = svc.getCharacteristics(UUID_CHAR_HRM_CONTROL)[0]
        logging.debug(
            "char_hrm_ctrl: %s [%x]", self.char_hrm_ctrl, self.char_hrm_ctrl.getHandle())
        self.char_hrm = svc.getCharacteristics(UUID_CHAR_HRM_MEASURE)[0]
        logging.debug("char_hrm: %s [%x]",
                      self.char_hrm, self.char_hrm.getHandle())
        self.cccd_hrm = self.char_hrm.getDescriptors(forUUID=CCCD_UUID)[0]

        svc_1 = self.getServiceByUUID(UUID_SVC_MIBAND1)
        logging.debug("svc_1: %s", svc_1)
        self.char_sensor = svc_1.getCharacteristics(UUID_CHAR_SENSOR)[0]  # 35
        logging.debug(
            "char_sensor: %s [%x]", self.char_sensor, self.char_sensor.getHandle())

        self.dataType = DATA_TYPE_NONE

        self.timeout = 5.0
        self.state = None
        # Enable auth service notifications on startup
        self.auth_notif(True)
        self.waitForNotifications(0.1)  # Let Mi Band to settle

    def init_after_auth(self):
        self.cccd_hrm.write(b"\x01\x00", True)

    def encrypt(self, message):
        aes = AES.new(self._KEY, AES.MODE_ECB)
        return aes.encrypt(message)

    def auth_notif(self, status):
        if status:
            logging.debug("Enabling Auth Service notifications status...")
            self.cccd_auth.write(b"\x01\x00", True)
        elif not status:
            logging.debug("Disabling Auth Service notifications status...")
            self.cccd_auth.write(b"\x00\x00", True)
        else:
            logging.error(
                "Something went wrong while changing the Auth Service notifications status...")

    def send_key(self):
        logging.debug("Sending Key...")
        self.char_auth.write(self._send_key_cmd)
        self.waitForNotifications(self.timeout)

    def req_rdn(self):
        logging.debug("Requesting random number...")
        self.char_auth.write(self._send_rnd_cmd)
        self.waitForNotifications(self.timeout)

    def send_enc_rdn(self, data):
        logging.debug("Sending encrypted random number")
        cmd = self._send_enc_key + self.encrypt(data)
        send_cmd = struct.pack('<18s', cmd)
        self.char_auth.write(send_cmd)
        self.waitForNotifications(self.timeout)

    def initialize(self):
        self.setDelegate(AuthenticationDelegate(self))
        self.send_key()

        while True:
            self.waitForNotifications(0.1)
            if self.state == "AUTHENTICATED":
                return True
            elif self.state:
                return False

    def authenticate(self):
        self.setDelegate(AuthenticationDelegate(self))
        self.req_rdn()

        while True:
            self.waitForNotifications(0.1)
            if self.state == "AUTHENTICATED":
                return True
            elif self.state:
                return False

    def hrmStopContinuous(self):
        logging.debug("hrmStopContinuous")
        self.char_hrm_ctrl.write(b'\x15\x01\x00', True)

    def hrmStartContinuous(self):
        logging.debug("hrmStartContinuous")
        self.char_hrm_ctrl.write(b'\x15\x01\x01', True)

    def writeHandle(self, handle, data, reply=False):
        logging.debug("writeHandle %x %s", handle, data)
        self.writeCharacteristic(handle, data, reply)

    def sensorStartContinuous(self):
        self.dataType = DATA_TYPE_PPG
        logging.debug("Enable PPG sensor")

        self.writeHandle(0x36, b'\x01\x00', True)  # 36 01 00

        logging.debug("Enable PPG raw data")
        self.char_sensor.write(b'\x01\x02\x19')  # 35 01 02 19

        logging.debug("Stop heart continuous")
        self.char_hrm_ctrl.write(b'\x15\x01\x00', True)  # 2C 15 01 00

        logging.debug("Start heart continuous")
        self.char_hrm_ctrl.write(b'\x15\x01\x01', True)  # 2C 15 01 01

        logging.debug("Start sensor data")
        self.char_sensor.write(b'\x02')                 # 35 02

    def sensorStopContinuous(self):
        logging.debug("sensorStopContinuous")

        logging.debug("Stop sensor data")
        self.char_sensor.write(b'\x03')

        logging.debug("Stop heart continuous")
        self.char_hrm_ctrl.write(b'\x15\x01\x00', True)  # 2C 15 01 00

    def hasPPGData(self, data):
        # Decide what you want to do with the data, here
        print(data)

    def accStartContinuous(self):
        self.dataType = DATA_TYPE_ACC
        logging.debug("Enable PPG sensor")

        # 39 01 00

        self.writeHandle(0x36, b'\x01\x00', True)  # 36 01 00

        logging.debug("Enable PPG raw data")
        self.char_sensor.write(b'\x01\x01\x19')  # 35 01 01 19

        # 36 00 00

        logging.debug("Start sensor data")
        self.char_sensor.write(b'\x02')                 # 35 02

    def accStopContinuous(self):
        logging.debug("accStopContinuous")
        logging.debug("Stop sensor data")
        self.char_sensor.write(b'\x03')

    def hasAccelerometerData(self, data):
        # Decide what you want to do with the data, here
        print(data)


class AuthenticationDelegate(DefaultDelegate):

    """This Class inherits DefaultDelegate to handle the authentication process."""

    def __init__(self, device):
        DefaultDelegate.__init__(self)
        self.device = device

    def _parse_raw_accel(self, bytes):
        res = []
        for i in range(3):
            g = struct.unpack('hhh', bytes[2 + i * 6:8 + i * 6])
            res.append({'x': g[0], 'y': g[1], 'z': g[2]})
        return res

    def _parse_raw_heart(self, bytes):
        #logging.debug("_parse_raw_heart: %s",  bytes)
        data_len = len(bytes)
        data_points = (data_len-2)//2
        res = []
        for i in range(data_points):
            offset = 2*(i+1)
            res += struct.unpack('H', bytes[offset:offset+2])
        return res

    def handleNotification(self, hnd, data):
        # Debug purposes
        #logging.debug("HANDLE: %s", str(hex(hnd)))
        #logging.debug("DATA: %s",  data)
        if hnd == self.device.char_auth.getHandle():
            if data[:3] == b'\x10\x01\x01':
                self.device.req_rdn()
            elif data[:3] == b'\x10\x01\x04':
                self.device.state = "ERROR: Key Sending failed"
            elif data[:3] == b'\x10\x02\x01':
                random_nr = data[3:]
                self.device.send_enc_rdn(random_nr)
            elif data[:3] == b'\x10\x02\x04':
                self.device.state = "ERROR: Something wrong when requesting the random number..."
            elif data[:3] == b'\x10\x03\x01':
                logging.info("Authenticated!")
                self.device.state = "AUTHENTICATED"
            elif data[:3] == b'\x10\x03\x04':
                logging.warning("Encryption Key Auth Fail, sending new key...")
                self.device.send_key()
            else:
                self.device.state = "ERROR: Auth failed"
        elif hnd == self.device.char_hrm.getHandle():
            rate = struct.unpack('bb', data)[1]
            print("Heart Rate: " + str(rate))
        elif hnd == 0x38:  # Raw data
            data_len = len(data)
            if self.device.dataType == DATA_TYPE_PPG:
                res = self._parse_raw_heart(data)
                logging.debug("Raw PPG: %s", res)
                self.device.hasPPGData(res)
            elif data_len == 20:
                res = self._parse_raw_accel(data)
                logging.debug("Raw Accel: %s", res)
                self.device.hasAccelerometerData(res)
            else:
                logging.debug("Unknown raw data: %s", data)
        else:
            logging.warning("Unhandled Response %s : %s", hex(hnd), data)


def main():
    """ main func """
    parser = argparse.ArgumentParser()
    parser.add_argument('host', action='store', help='MAC of BT device')
    parser.add_argument('-t', action='store', type=float, default=3.0,
                        help='duration of each notification')

    parser.add_argument('--init', action='store_true', default=False)
    parser.add_argument('-n', '--notify', action='store_true', default=False)
    parser.add_argument('-hrm', '--heart', action='store_true', default=False)
    parser.add_argument('-ppg', '--PPG_sensor',
                        action='store_true', default=False)
    parser.add_argument('-a', '--accelerometer',
                        action='store_true', default=False)
    parser.add_argument('-d', '--debug', type=int, default=0,
                        help='Enable debugging when greater than 0')
    arg = parser.parse_args(sys.argv[1:])

    FORMAT = '%(asctime)s.%(msecs)03d-%(levelname)s: %(message)s'
    if arg.debug == 0:
        logging.basicConfig(
            format=FORMAT, datefmt='%H:%M:%S', level=logging.INFO)
    else:
        logging.basicConfig(
            format=FORMAT, datefmt='%H:%M:%S', level=logging.DEBUG)

    logging.info('Connecting to %s', arg.host)
    band = MiBand2(arg.host)
    band.setSecurityLevel(level="medium")

    if arg.init:
        if band.initialize():
            print("Init OK")
        band.disconnect()
        return
    else:
        band.authenticate()

    band.init_after_auth()

    if arg.notify:
        print("Sending message notification...")
        band.char_alert.write(b'\x01')
        time.sleep(arg.t)
        print("Sending phone notification...")
        band.char_alert.write(b'\x02')
        time.sleep(arg.t)
        print("Turning off notifications...")
        band.char_alert.write(b'\x00')

    if arg.heart:
        print("Cont. HRM start")
        band.hrmStopContinuous()
        band.hrmStartContinuous()
        for i in range(30):
            band.waitForNotifications(1.0)

    if arg.PPG_sensor:
        print("PPG Raw Data start")
        band.sensorStartContinuous()
        ping_time = datetime.now() + timedelta(seconds=10)
        stop_time = datetime.now() + timedelta(seconds=30)
        now_time = datetime.now()
        while now_time < stop_time:
            band.waitForNotifications(1.0)
            if now_time > ping_time:
                logging.debug("Writing ping")
                band.char_hrm_ctrl.write(b'\x16', True)
                ping_time += timedelta(seconds=10)
            now_time = datetime.now()
        band.sensorStopContinuous()

    if arg.accelerometer:
        print("Accelerometer Raw Data start")
        band.accStartContinuous()
        ping_time = datetime.now() + timedelta(seconds=10)
        stop_time = datetime.now() + timedelta(seconds=30)
        now_time = datetime.now()
        while now_time < stop_time:
            band.waitForNotifications(1.0)
            if now_time > ping_time:
                logging.debug("Writing ping")
                band.char_hrm_ctrl.write(b'\x16', True)
                ping_time += timedelta(seconds=10)
            now_time = datetime.now()
        band.accStopContinuous()

    logging.info("Disconnecting...")
    band.disconnect()
    del band


if __name__ == "__main__":
    main()
