# Mi Band 2 Python
Reading data from the [**Mi Band 2**](http://www.mi.com/en/miband2/) using Python under Linux.

![demo](/demo.png)

![Raw Data](/raw_data.png)

## Setting up
You need to unpair your Mi Band 2 from every application (MiFit App, GadgetBridge, Master for Mi Band, etc) first.
You should be able to bind it back again, but no guaranee here.

When you first connect to your Mi Band 2, you will need to accept pairing on the
Mi Band 2.

This script uses the default authentication key "0123456789@ABCDE". If you have
not changed the key, e.g. you are using "Own Authentication" for Master for Mi Band,
you might not lose any data from the Mi Band 2, but it is best to synchronise 
data before unpairing.


## Running the Program
You can run the program to read the processed heartbeat:
```sh
miband2.py YOUR_MAC --heart
```

Or you can run the program to read the raw [Photoplethysmogram (PPG)](https://en.wikipedia.org/wiki/Photoplethysmogram) data:
```sh
miband2.py YOUR_MAC --ppg
```

## Installation
Install `Python 3` and the `bluepy` library:
```sh
pip3 install bluepy --user
```

## Credits
This is a fixed and improved version of the script, provided by 
[Volodymyr Shymanskyy](https://github.com/vshymanskyy/miband2-python-test), 
which is in turn, a modified version of the original script written by 
[Leo Soares](https://github.com/leojrfs/miband2). 
Leo also wrote a nice article here: [Mi Band 2, Part 1: Authentication](https://leojrfs.github.io/writing/miband2-part1-auth/)

I also borrowed heavily from 
[Andrey Nikishaev's article](https://medium.com/machine-learning-world/how-i-hacked-xiaomi-miband-2-to-control-it-from-linux-a5bd2f36d3ad), with some additional help from snooping the Bluetooth communication
from [Master for Mi Band](https://play.google.com/store/apps/details?id=blacknote.mibandmaster)

